eos-monitor v2
===========

# 功能

通过Ajax请求[https://tokenclub.com/coin/eos](https://tokenclub.com/coin/eos)上的EOS兑CNY报价，价格高于或低于预设的价格区间时进行邮件提醒

# 新版本的改变(20190329)

- 新增机能：可视化图表支持绘制设定的价格上下限
- 新增机能：连接被切断时避免程序结束，尝试等待30min后再度执行
- 需要更新的文件：main.py class_parser.py visualization.py

# 依赖

+ Python3 [下载](https://www.python.org/downloads/) [安装(安装包安装)](https://germey.gitbooks.io/python3webspider/content/1.1-Python3%E7%9A%84%E5%AE%89%E8%A3%85.html)
+ Request [安装(pip安装)](https://germey.gitbooks.io/python3webspider/content/1.2.1-Requests%E7%9A%84%E5%AE%89%E8%A3%85.html)
+ plotly [安装(pip安装)](https://plot.ly/~xianhu/0/pip-install-plotly-plotl/#/)

# 使用

下载v2文件夹下的5个文件并确保在同一目录下，编辑配置文件config后，执行main.py

显示报价的同时会将时间和价格记录在log.csv中，执行visualization.py会读取log.csv并在浏览器中显示图表

## 配置文件说明
HighPrice：价格区间高值  
LowPrice：价格区间低值  
Loop：是否循环  
TimeSleep：循环时间间隔（秒）  
SMTP_Address：发件邮箱的SMTP服务地址  
SMTP_Port：发件邮箱的SMTP服务端口  
Sender：发件邮箱  
Password：发件邮箱密码  
Receiver：收件邮箱  
Subject：邮件主题

## Linux & macOS

假设所有依赖安装完成且main.py文件存在于`~/eos/`，那么在终端执行

```shell
cd ~/eos
python3 main.py
```

## Windows

假设所有依赖安装完成且main.py文件存在于`C://eos`，那么在控制台执行

```shell
cd c://eos
python3.exe main.py
```
