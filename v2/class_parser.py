import requests


class Parser():
    def __init__(self):
        self.base_url = 'https://api.schail.com/v1/ticker/summary/detail/?id=eos'
        self.headers = {
            'Origin': 'https://tokenclub.com',
            'Referer': 'https://tokenclub.com/coin/eos',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3733.0 Safari/537.36',
            'version': '4.0.1',
        }

    def get_page(self):
        try:
            response = requests.get(self.base_url, headers=self.headers)
            if response.status_code == 200:
                return response.json()
        except requests.ConnectionError as e:
            print('请求页面失败')
            print(e.args)

    def parse_page(self, json):
        if json:
            items = json.get('data')
            price = float(items['detail']['price'])
            usd_rate = float(items['rate'])
            return price * usd_rate
        else:
            return -1
