import smtplib
from email.mime.text import MIMEText
import configparser


class Mail():
    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config', encoding='utf-8')
        self.sender_smtp_address = config['MailNotification']['SMTP_Address']
        self.sender_smtp_port = int(config['MailNotification']['SMTP_Port'])
        self.sender = config['MailNotification']['Sender']
        self.password = config['MailNotification']['Password']
        self.receiver = config['MailNotification']['Receiver']
        self.subject = config['MailNotification']['Subject']

    def mail(self, content):
        ret = True
        try:
            msg = MIMEText(content, 'plain', 'utf-8')
            msg['From'] = self.sender
            msg['To'] = self.receiver
            msg['Subject'] = self.subject
            server = smtplib.SMTP_SSL(self.sender_smtp_address, self.sender_smtp_port)
            server.login(self.sender, self.password)
            server.sendmail(self.sender, [self.receiver, ], msg.as_string())
            server.quit()
            print("邮件发送成功\n")
        except Exception:
            ret = False
            print("邮件发送失败\n")
        return ret
