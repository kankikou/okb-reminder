import requests
import time
import threading
import sys
import winsound

args = sys.argv

url_eos = 'https://www.okb.com/v2/spot/markets/tickers'
url_usd2cny = 'https://www.okb.com/v2/futures/market/indexTickerAll.do'

headers_usd2cny = {
    'Host': 'www.okb.com',
    'Referer': 'https://www.okb.com/marketList',
    'Connection': 'keep-alive',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
    'X-Requested-With': 'XMLHttpRequest',
    'Cookie': '_ga=GA1.2.572728249.1530522288; _gid=GA1.2.1509264208.1530522288; perm=A364DD49D354EDA46B23976B747C8C85; lp=; Hm_lvt_b4e1f9d04a77cfd5db302bc2bcc6fe45=1530522289; __zlcmid=nChSZMJiQJtcna; ref=https://www.okb.com/marketList; first_ref=https://www.okb.com/marketList; locale=en_US; _gat_gtag_UA_115738092_1=1; Hm_lpvt_b4e1f9d04a77cfd5db302bc2bcc6fe45=1530583975'
}

headers_eos = {
    'Host': 'www.okb.com',
    'Referer': 'https://www.okb.com/marketList',
    'Connection': 'keep-alive',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
    'X-Requested-With': 'XMLHttpRequest',
    'Cookie': '_ga=GA1.2.572728249.1530522288; _gid=GA1.2.1509264208.1530522288; perm=A364DD49D354EDA46B23976B747C8C85; lp=; Hm_lvt_b4e1f9d04a77cfd5db302bc2bcc6fe45=1530522289; __zlcmid=nChSZMJiQJtcna; ref=https://www.okb.com/marketList; first_ref=https://www.okb.com/marketList; locale=en_US; _gat_gtag_UA_115738092_1=1; Hm_lpvt_b4e1f9d04a77cfd5db302bc2bcc6fe45=1530543498'
}


def get_page():
    try:
        response = requests.get(url_eos, headers=headers_eos)
        if response.status_code == 200:
            return response.json()
    except requests.ConnectionError as e:
        print('Error', e.args)


def get_usd2cny():
    try:
        response = requests.get(url_usd2cny, headers=headers_usd2cny)
        if response.status_code == 200:
            return response.json()
    except requests.ConnectionError as e:
        print('Error', e.args)


def parse_page(json, index):
    if json:
        items = json.get('data')
        infos = []
        for i in index:
            item = items[i]
            info = {}
            info['symbol'] = item.get('symbol')
            info['buy'] = item.get('buy')
            info['sell'] = item.get('sell')
            infos.append(info)
    return infos


def parse_usd2cny(json):
    if json:
        items = json.get('data')
        item = items[0]
        usdCnyRate = item['usdCnyRate']
    return float(usdCnyRate)


def schedule(interval, info, index, wait=True):
    base_time = time.time()
    next_time = 0
    while True:
        t = threading.Thread(target=job(info, index))
        t.start()
        if wait:
            t.join()
        next_time = ((base_time - time.time()) % interval) or interval
        time.sleep(next_time)


def job(info, index):
    request_json = get_page()
    info_dict_list = parse_page(request_json, index)
    rate_info = get_usd2cny()
    rate = parse_usd2cny(rate_info)
    for i in range(len(index)):
        buy = float(info_dict_list[i]['buy']) * rate
        sell = float(info_dict_list[i]['sell']) * rate
        print('currency='+info_dict_list[i]['symbol']+'  buy=￥'+str(buy)+'  sell=￥'+str(sell))
        if sell > info['high']:
            print('sell > high')
            winsound.beep(1000, 1000)
        if sell < info['low']:
            print('sell < low')
            winsound.beep(1000, 1000)


if __name__ == '__main__':
    print('low=￥'+args[1]+'\t high=￥'+args[2])
    infomation = {}
    infomation['low'] = float(args[1])
    infomation['high'] = float(args[2])
    request_json = get_page()
    items = request_json.get('data')
    watch_index = []
    for item in items:
        if item.get('symbol') == 'eos_usdt':
            watch_index.append(items.index(item))
    schedule(10, infomation, watch_index, True)
