okb-reminder v1 readme
===========

# 功能

通过Ajax请求[https://www.okb.com/marketList](https://www.okb.com/marketList)上的EOS兑美元及美元兑人民币的汇率信息，得到EOS兑CNY的报价，每隔一段时间刷新EOS的CNY币值并输出到控制台，超出预设的范围即提醒

# 依赖

+ Python3 [下载](https://www.python.org/downloads/) [安装(安装包安装)](https://germey.gitbooks.io/python3webspider/content/1.1-Python3%E7%9A%84%E5%AE%89%E8%A3%85.html)
+ Request [安装(pip安装)](https://germey.gitbooks.io/python3webspider/content/1.2.1-Requests%E7%9A%84%E5%AE%89%E8%A3%85.html)

# 参数

## 对于v1

`python3 okb_reminder_v1.py 64 89`设定安全阈值在64～89CNY，超出即提醒

## 对于v1.5beta及以上版本

`python3 okb_reminder_v1.py -t 20 -l 64 -h 89`每20s刷新一次，设定安全阈值在64～89CNY，超出即提醒

# 使用

## Linux & macOS

假设所有依赖安装完成且okb_reminder_v1.py文件存在于`~/obk/`，那么在终端执行

```shell
cd ~/obk
python3 okb_reminder_v1.py 64 89
```

## Windows

假设所有依赖安装完成且okb_reminder的.py文件存在于`C://aaa/bbb`，那么在控制台执行

```shell
cd c://aaa/bbb
python3.exe okb_reminder_v1.py 64 89
```
