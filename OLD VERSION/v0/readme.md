okb-reminder_v0
===========

# 功能

通过GeckoDriver调用Firefox打开[https://www.okb.com/marketList](https://www.okb.com/marketList)，选择币种为CNY，每隔9s刷新EOS币值并输出到控制台

# 依赖

+ Firefox浏览器 [下载](https://www.mozilla.org/en-US/firefox/new/)
+ Python3 [下载](https://www.python.org/downloads/)
+ Selenium [安装方法](https://germey.gitbooks.io/python3webspider/content/1.2.2-Selenium%E7%9A%84%E5%AE%89%E8%A3%85.html)
+ GeckoDriver [安装方法](https://germey.gitbooks.io/python3webspider/content/1.2.4-GeckoDriver%E7%9A%84%E5%AE%89%E8%A3%85.html)

# 使用

## Linux & macOS
假设所有依赖安装完成且okb_reminder.py文件存在于`~/obk/`，那么在终端执行

```shell
cd ~/obk
python3 okb_reminder.py
```

## Windows
假设所有依赖安装完成且okb_reminder.py文件存在于`C://aaa/bbb`，那么在控制台执行

```shell
cd c://aaa/bbb
python3.exe okb_reminder.py
```
