from selenium import webdriver
import time

browser = webdriver.Firefox()
browser.get('https://www.okb.com/marketList')
change_language_button = browser.find_element_by_id('chooseCurrBase')
change_language_button.click()
tickers = browser.find_element_by_class_name('market-list-tickers')
while True:
    text = tickers.text  # <class 'str'>
    # ticker_infos = text.split('\n')
    # EOS_info = ticker_infos[44].split(' ')
    EOS_price = float(text.split('\n')[44].split(' ')[1].split('￥')[1])
    print(EOS_price)
    time.sleep(1)
browser.close()
